from plot import plot
from snake_game import SnakeGame

import random
import numpy as np
from tensorflow.keras import Sequential
from collections import deque
from tensorflow.keras.layers import Dense
from tensorflow.keras.optimizers import Adam
import tensorflow as tf
import shelve

MODEL_DIRECTORY = './models/'
MEMORIES = 'model-memories'


class SnakeAgent:
    def __init__(self, params):
        self.action_space = params['action_space']
        self.state_space = params['state_space']
        self.epsilon = params['epsilon']
        self.epsilon_min = params['epsilon_min']
        self.epsilon_decay = params['epsilon_decay']
        self.random_move_mode = params['random_move_mode']
        self.gamma = params['gamma']
        self.batch_size = params['batch_size']
        self.learning_rate = params['learning_rate'] if params['learn_mode'] else 0
        self.model_name = params['model_name']
        self.base_model = params['base_model']
        self.save_freq = params['save_freq']
        self.plot_freq = params['plot_freq']
        shelf = shelve.open(MEMORIES)
        if self.base_model not in shelf:
            shelf[self.base_model] = deque(maxlen=2500)
        self.memory = shelf[self.base_model]
        shelf.close()
        self.model = self.create_model()
        self.target_model = self.create_model()

    def create_model(self):
        model = None
        try:
            model = tf.keras.models.load_model(
                MODEL_DIRECTORY + self.base_model)
        except:
            print('could not load weights, starting fresh')
            model = Sequential(layers=[
                Dense(1024, input_shape=(
                    self.state_space,), activation='relu'),
                Dense(1024, activation='relu'),
                Dense(1024, activation='relu'),
                Dense(self.action_space, activation='sigmoid')
            ])
        model.compile(loss='mse', optimizer=Adam(
            lr=self.learning_rate))
        return model

    def remember(self, state, action, reward, next_state, done):
        self.memory.append((state, action, reward, next_state, done))

    def act(self, state, env):
        if np.random.rand() <= self.epsilon:
            return random.randrange(self.action_space) if self.random_move_mode else env.safest_move_to_food_action()
        act_values = self.model.predict(state)
        return np.argmax(act_values[0])

    def replay(self):

        if len(self.memory) < self.batch_size:
            return

        minibatch = random.sample(self.memory, self.batch_size)
        states = np.array([i[0] for i in minibatch])
        actions = np.array([i[1] for i in minibatch])
        rewards = np.array([i[2] for i in minibatch])
        next_states = np.array([i[3] for i in minibatch])
        dones = np.array([i[4] for i in minibatch])
        states = np.squeeze(states)
        next_states = np.squeeze(next_states)

        targets = rewards + self.gamma * \
            (np.amax(self.target_model.predict_on_batch(next_states), axis=1))*(1-dones)
        targets_full = self.target_model.predict_on_batch(states)

        ind = np.array([i for i in range(self.batch_size)])
        targets_full[[ind], [actions]] = targets

        self.model.fit(states, targets_full, epochs=1,
                       verbose=0)
        if self.epsilon > self.epsilon_min:
            self.epsilon *= self.epsilon_decay

    def target_train(self):
        weights = self.model.get_weights()
        target_weights = self.target_model.get_weights()
        for i in range(len(target_weights)):
            target_weights[i] = weights[i]
            self.target_model.set_weights(target_weights)


def train_dqn(epoch):
    sum_of_rewards = []
    agent = SnakeAgent(params)
    scores = {
        'game_scores': [],
        'mean_scores': [],
        'mean_scores_last_ten': [],
        'mean_scores_last_twenty_five': [],
        'total_scores': 0
    }
    for e in range(epoch):
        env = SnakeGame()
        state = env.get_state()

        state = np.reshape(state, (1, agent.state_space))
        score = 0
        max_steps = 10000
        for i in range(max_steps):
            action = agent.act(state, env)
            done, reward, next_state, current_score = env.play_step(action)
            score += reward
            next_state = np.reshape(next_state, (1, agent.state_space))
            agent.remember(state, action, reward, next_state, done)
            state = next_state
            agent.replay()
            if done:
                scores['game_scores'].append(current_score)
                scores['total_scores'] += current_score
                scores['mean_scores'].append(scores['total_scores'] / (e + 1))
                scores['mean_scores_last_ten'].append(
                    np.sum(scores['game_scores'][-10:]) / 10 if len(scores['game_scores']) > 10 else 0)
                scores['mean_scores_last_twenty_five'].append(
                    np.sum(scores['game_scores'][-25:]) / 25 if len(scores['game_scores']) > 25 else 0)

                if (e + 1) % agent.plot_freq == 0:
                    plot(params['model_name'], scores)
                if (e + 1) % agent.save_freq == 0:
                    agent.model.save(MODEL_DIRECTORY + agent.model_name)
                    shelf = shelve.open(MEMORIES)
                    shelf[agent.model_name] = agent.memory
                    shelf.close()

                agent.target_train()
                print(f'epoch: {e+1}/{epoch}, score: {score}')
                break
        sum_of_rewards.append(score)
    return sum_of_rewards


if __name__ == '__main__':

    params = dict()
    params['name'] = None
    params['epsilon'] = 0  # 0.99
    params['epsilon_min'] = 0.05
    params['epsilon_decay'] = .99
    params['random_move_mode'] = False  # False

    params['learning_rate'] = 0.00025  # 0.00025
    params['learn_mode'] = False  # True

    params['gamma'] = .95
    params['batch_size'] = 500
    params['state_space'] = 34  # 14
    params['action_space'] = 3

    params['plot_freq'] = 25
    params['save_freq'] = 50

    params['model_name'] = 'SNAKE_AI_V7.2.large'
    params['base_model'] = 'SNAKE_AI_V7.2'

    results = dict()
    ep = 3000
    sum_of_rewards = train_dqn(ep)
