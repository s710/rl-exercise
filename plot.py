import matplotlib.pyplot as plt
from IPython import display

plt.ion()

PLOT_SAVE_DIRECTORY = './learning_plots/'
SCORES_TO_PLOT = ["game_scores", "mean_scores",
                  "mean_scores_last_ten", "mean_scores_last_twenty_five"]


def plot(model_name, scores):
    display.clear_output(wait=True)
    display.display(plt.gcf())
    plt.clf()
    plt.title('Stats this training session for ' + model_name)
    plt.xlabel('Number of Games')
    plt.ylabel('Score')
    for score in SCORES_TO_PLOT:
        plt.plot(scores[score])
        plt.text(len(scores[score])-1, scores[score]
                 [-1], str(scores[score][-1]))
    plt.ylim(ymin=0)
    plt.show(block=False)
    plt.savefig(PLOT_SAVE_DIRECTORY + model_name +
                '.png', bbox_inches='tight')
    plt.pause(.1)
