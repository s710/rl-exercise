import pygame
import random
from enum import Enum
from collections import namedtuple
import numpy as np
from tensorflow.python.keras.backend import log
pygame.init()

font = pygame.font.SysFont('arial.ttf', 25)


class Direction(Enum):
    RIGHT = 1
    LEFT = 2
    UP = 3
    DOWN = 4


Point = namedtuple('Point', 'x, y')
Slope = namedtuple('Slope', 'x, y')

# rgb colors
WHITE = (255, 255, 255)
RED = (200, 0, 0)
BLUE1 = (0, 0, 255)
BLUE2 = (0, 100, 255)
BLACK = (0, 0, 0)

BLOCK_SIZE = 20
SPEED = 10000000

EAT_REWARD = 10

STEP_PENALTY = 0
DEATH_PENALTY = -10
BEEN_HERE_BEFORE_PENALTY = -1

MAX_MOVES_WITHOUT_EATING = 50

HEADLESS = False

WIDTH = 32  # 32
HEIGHT = 24  # 24


class SnakeGame:

    def __init__(self, w=WIDTH * BLOCK_SIZE, h=HEIGHT * BLOCK_SIZE):
        self.w = w
        self.h = h
        if not HEADLESS:
            self.display = pygame.display.set_mode((self.w, self.h))
            pygame.display.set_caption('Snake')
        self.clock = pygame.time.Clock()
        self.tail_direction = Direction.RIGHT
        self.direction = Direction.RIGHT
        self.head = Point(self.w/2, self.h/2)
        self.snake = [self.head,
                      Point(self.head.x-BLOCK_SIZE, self.head.y),
                      Point(self.head.x-(2*BLOCK_SIZE), self.head.y)]
        self.score = 0
        self.moveWithoutEating = 0
        self.pointsHistoryAfterLastEat = []
        self.food = None
        self.steps = 0
        self._place_food()

    def _place_food(self):
        x = random.randint(0, (self.w-BLOCK_SIZE)//BLOCK_SIZE)*BLOCK_SIZE
        y = random.randint(0, (self.h-BLOCK_SIZE)//BLOCK_SIZE)*BLOCK_SIZE
        self.food = Point(x, y)
        if self.food in self.snake:
            self._place_food()

    def play_step(self, action):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()
            if event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE:
                paused = True
                while paused:
                    for event in pygame.event.get():
                        if event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE:
                            paused = False

        self._move(action)
        self.snake.insert(0, self.head)
        self.steps += 1

        def die():
            return True, DEATH_PENALTY, self.get_state(), self.score

        done = False
        reward = STEP_PENALTY
        if self._is_collision():
            return die()
        if self._is_apple_location(self.head):
            self.score += 1
            reward += EAT_REWARD
            self._place_food()
            self.moveWithoutEating = 0
            self.pointsHistoryAfterLastEat = []
        else:
            if len(self.pointsHistoryAfterLastEat) >= WIDTH * HEIGHT:
                print('death because of not eating')
                # return die()
            if self._been_here_before():
                reward += BEEN_HERE_BEFORE_PENALTY
            self.pointsHistoryAfterLastEat.append(self.snake[0])
            self.moveWithoutEating += 1
            self.snake.pop()
        self._update_ui()
        self.clock.tick(SPEED)

        return done, reward, self.get_state(), self.score

    def _is_apple_location(self, pt: Point):
        return pt == self.food

    def _is_body_location(self, pt: Point):
        return pt in self.snake[1:]

    def _wall_collision(self, pt: Point):
        return pt.x > self.w - BLOCK_SIZE or pt.x < 0 or pt.y > self.h - BLOCK_SIZE or pt.y < 0

    def _is_collision(self, pt=None):
        if pt is None:
            pt = self.head
        return self._wall_collision(pt) or self._is_body_location(pt)

    def _update_ui(self):
        if HEADLESS:
            return
        self.display.fill(BLACK)

        for pt in self.snake:
            pygame.draw.rect(self.display, BLUE1, pygame.Rect(
                pt.x, pt.y, BLOCK_SIZE, BLOCK_SIZE))
            pygame.draw.rect(self.display, BLUE2,
                             pygame.Rect(pt.x+4, pt.y+4, 12, 12))

        pygame.draw.rect(self.display, RED, pygame.Rect(
            self.food.x, self.food.y, BLOCK_SIZE, BLOCK_SIZE))

        text = font.render("Score: " + str(self.score), True, WHITE)
        self.display.blit(text, [0, 0])
        pygame.display.flip()

    def _been_here_before(self):
        return self.snake[0] in self.pointsHistoryAfterLastEat[:-1]

    def _moving_towards_food(self, direction=None):
        if direction is None:
            direction = self.direction
        is_moving_towards_food = {
            Direction.LEFT: self.food.x < self.snake[0].x,
            Direction.RIGHT: self.food.x > self.snake[0].x,
            Direction.DOWN: self.food.y < self.snake[0].y,
            Direction.UP: self.food.y > self.snake[0].y,
        }
        return is_moving_towards_food[direction]

    def safest_move_to_food_action(self):
        good_actions = []
        for action in [0, 1, 2]:
            direction = self.direction_for_action(action)
            point_in_direction = self.point_in_direction(direction)
            good_actions.append([self._moving_towards_food(
                direction), not self._is_collision(point_in_direction)])
        return good_actions.index(sorted(good_actions, key=lambda x: x.count(True), reverse=True)[0])

    def _move(self, action):
        self.tail_direction = self.direction
        self.direction = self.direction_for_action(action)
        self.head = self.point_in_direction(self.direction)

    def direction_for_action(self, action):
        clockwise = [Direction.UP, Direction.RIGHT,
                     Direction.DOWN, Direction.LEFT]
        direction_index = clockwise.index(self.direction)
        actions = {
            0: clockwise[direction_index],
            1: clockwise[(direction_index + 1) % 4],
            2: clockwise[(direction_index - 1) % 4],
        }
        return actions[action]

    def point_in_direction(self, direction):
        directions = {
            Direction.RIGHT: Point(self.head.x + BLOCK_SIZE, self.head.y),
            Direction.LEFT: Point(self.head.x - BLOCK_SIZE, self.head.y),
            Direction.UP: Point(self.head.x, self.head.y + BLOCK_SIZE),
            Direction.DOWN: Point(self.head.x, self.head.y - BLOCK_SIZE),
        }
        return directions[direction]

    def vision(self):
        slope_in_order = [(-1, 1), (0, 1), (1, 1), (-1, 0),
                          (1, 0), (-1, -1), (0, -1), (1, -1)]
        vision = []
        for slope in slope_in_order:
            vision.extend(self.vision_in_direction(
                Point(slope[0] * BLOCK_SIZE, slope[1] * BLOCK_SIZE)))
        return vision

    def vision_in_direction(self, slope: Slope):
        dist_to_wall = None
        dist_to_apple = np.inf
        dist_to_self = np.inf

        position = Point(self.head.x + slope.x, self.head.y + slope.y)
        distance = 1.0
        total_distance = distance
        body_found = False
        food_found = False

        while not self._wall_collision(position):
            if not body_found and self._is_body_location(position):
                dist_to_self = total_distance
                body_found = True
            if not food_found and self._is_apple_location(position):
                dist_to_apple = total_distance
                food_found = True
            position = Point(position.x + slope.x, position.y + slope.y)
            total_distance += distance

        dist_to_wall = 1.0 / total_distance
        dist_to_apple = 1.0 / dist_to_apple
        dist_to_self = 1.0 / dist_to_self
        return [dist_to_wall, dist_to_apple, dist_to_self]

    def get_state(self):
        snake_head = self.snake[0]
        food = self.food
        dir = self.direction
        tail_dir = self.tail_direction
        dir_l, dir_r, dir_u, dir_d = dir == Direction.LEFT, dir == Direction.RIGHT, dir == Direction.UP, dir == Direction.DOWN
        tail_dir_l, tail_dir_r, tail_dir_u, tail_dir_d = tail_dir == Direction.LEFT, tail_dir == Direction.RIGHT, tail_dir == Direction.UP, tail_dir == Direction.DOWN
        state = self.vision() + [
            # (dir_l and self._is_collision(self.point_in_direction(Direction.LEFT))),
            # (dir_r and self._is_collision(self.point_in_direction(Direction.RIGHT))),
            # (dir_u and self._is_collision(self.point_in_direction(Direction.UP))),
            # (dir_d and self._is_collision(self.point_in_direction(Direction.DOWN))),
            int(dir_l),
            int(dir_r),
            int(dir_u),
            int(dir_d),

            int(tail_dir_l),
            int(tail_dir_r),
            int(tail_dir_u),
            int(tail_dir_d),

            # int(food.y > snake_head.y),
            # int(food.y < snake_head.y),
            # int(food.x < snake_head.x),
            # int(food.x > snake_head.x),

            # food.y / self.h,
            # food.x / self.w,
            # snake_head.x / self.w,
            # snake_head.y / self.h,

            int(self._moving_towards_food()),
            int(self._been_here_before())
        ]
        return state
