# Snake AI using dueling DQN for RL exercise by Zen Visser | 2021-2022
https://gitlab.com/s710/rl-exercise

## Goal

The goal of this project was to create a dueling DQN agent to play snake as well as possible.

## Methods

- A friend of mine gave me the base of the snake game (which did have to be modified a lot to be able to develop the DQN),
- Used the previous tutorial for a base of the DQN agent,
- Looked up other snake AI's for research,
- Updated DQN,
- Implemented features to get historic overview of agent performance while training,
- Implemented features to save/load models for faster training,
- Tweaked state/rewards/neural network structure.
- Added better vision system (which did not bring much better results)

## Results

In the learning_plots directory a few versions of models can be found, the first few versions have unfortunately been lost but before V1 there were a few models that scored quite high. In V1 I started to experiment with some different reward structures that showed to not work. From v2.0 on I used v2.0 to base the next model on, using its neural networks weights and last 2500 steps as base memory.

The plots have multiple lines; 
blue: score per game
yellow: total average at that game
green: average score over last 10 games
red: average score over last 25 games

### Features

- Saving the model every x epochs
- Saving the model's memory every x epochs
- Loading a model
- Pausing the game
- Plotting the stats of the model every x epochs
- HEADLESS mode for the game

### SNAKE_AI Version notes

- 2.4 showed an average score of 18 after 150 epochs which is pretty good, it used V2.0 to start from. 
- 3.0 didn't use any model to start from and changed some of the state. 
- 4.0 used 10x10 map for training, was based off 3.0 initially
- 4.1 used 32x24 map for training, was based off 4.0 and had disabled any form of learning or 'random' moves
- 6.0 trained on 10x10, using first (probably bad) version of vision (BASE: 6.0)
- 6.1 trained on 10x10, should now have better info about vision (BASE: 6.1)
- 6.2 trained on 10x10, should now have better previous state info (BASE: 6.2)
- 6.3 trained on 10x10, changed neural network structure (BASE: 6.3)
- 6.4 trained on 10x10, changed back neural network size, still using sigmoid for output activation (BASE: 6.4)
- 6.5 trained on 10x10, added tail direction, removed food up/down/left/right (BASE 6.5)
- 6.6 trained on 10x10 same as 6.5, but now using 2 relu layers with 20 and 12 nodes instead (BASE 6.5)
- 7 trained on 10x10 same as 6.5, but now using 2 relu layers with 1024 and 1024 nodes instead (BASE 6.5)
- 7.2 trained on 10x10, added third relu layer (BASE 6.5)
## Discussion

My AI got nowhere close near to 'solving' snake. But I'm very happy with the 'final' results.

There are alternatives and options I did not try for the state/neural network structure. Other neural network structures (like a CNN structure) could be used instead, or something like a genetic algorithm, supposedly the latter should be able to beat snake.

## References

https://github.com/benjamin-dupuis/DQN-snake

https://towardsdatascience.com/snake-played-by-a-deep-reinforcement-learning-agent-53f2c4331d36

https://www.youtube.com/watch?v=WjuLQVg04JY

https://www.youtube.com/watch?v=5Vy5Dxu7vDs

https://www.youtube.com/watch?v=WHU_Xtni2ag

https://awesomeopensource.com/project/YuriyGuts/snake-ai-reinforcement

https://openreview.net/pdf?id=iu2XOJ45cxo

https://stats.stackexchange.com/questions/464637/why-is-my-dqn-agent-not-learning-to-eat-its-food-simple-snake-game

https://s3.ap-northeast-2.amazonaws.com/journal-home/journal/jips/fullText/477/jips_v16n5_7.pdf

https://www.youtube.com/watch?v=vhiO4WsHA6c

